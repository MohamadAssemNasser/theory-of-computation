var graphOne = new GraphState();
var clickedCoordinatesS1 = {};
var clickedNodeS1 = -1;

var nodeIdS1 = 0,
    selectedNodeS1 = null,
    edgeIdS1 = 0;

let s1 = new sigma({
    settings,
    renderer: {
        container: document.getElementById("canvasOne"),
        type: "canvas",
    },
});

// ADD NODE ON STAGE DOUBLE CLICK
const addStateS1 = () => {
    if ($('#stateNameInput1').val() && graphOne.addNode($('#stateNameInput1').val().trim(), $('input[name=stateOption1]:checked').val() == '#fb3189' ? 'final' : 'normal')) {
        s1.graph.addNode({
            id: nodeIdS1,
            label: $('#stateNameInput1').val().trim(),
            x: clickedCoordinatesS1.x,
            y: clickedCoordinatesS1.y,
            color: $('input[name=stateOption1]:checked').val(),
            size: 25,
        });
        nodeIdS1 === 0 &&
            s1.graph.addEdge({
                id: "-1",
                source: "-1",
                target: "0",
                size: 2,
                type: "arrow",
            }) && graphOne.setInitialState($('#stateNameInput1').val().trim());
        selectedNodeS1 = null;
        $("#stateNameInput1").val("");
        $('#inputModal1').modal('hide');
    }
    nodeIdS1++;
}

const onBackgroundDoubleClickS1 = (e) => {
    clickedCoordinatesS1.x = e.data.captor.x;
    clickedCoordinatesS1.y = e.data.captor.y;
    $("#stateNameInput1").focus();
    $('#inputModal1').modal('show');
}
s1.bind('doubleClickStage', onBackgroundDoubleClickS1);

// DELETE NODE ON NODE DOUBLE CLICK
const deleteStateS1 = () => {
    const id = $('#stateName1').val();
    const name = $(`#stateName1 > option[value=${id}]`).html();
    if (id > 0) {
        graphOne.dropNode(name);
        s1.graph.dropNode(parseInt(id));
        $('#deleteStateModal1').modal('hide');
    }
}
const onNodeDoubleClickS1 = (e) => {
    if (e.data.captor.ctrlKey)
        return;
    let options = '';
    s1.graph.nodes().forEach((node) => {
        if (node.id !== e.data.node.id && node.id > -2)
            options = options + `<option value=${node.id}>${node.label}</option>`
    });
    options += `<option value=${e.data.node.id} selected>${e.data.node.label}</option>`
    $('#stateName1').html(options);
    $('#deleteStateModal1').modal('show');
}
s1.bind('doubleClickNode', onNodeDoubleClickS1);

// ADD EDGE ON NODE CLICK
const addEdgeS1 = (source, target, label) => {
    console.log(target)
    if (graphOne.addEdge(s1.graph.nodes(source).label, s1.graph.nodes(target).label, label)) {
        if (graphOne.edgeRequireConcat(s1.graph.nodes(source).label, s1.graph.nodes(target).label)) {
            s1.graph.edges().forEach((edge) => {
                if (edge.source === source && edge.target === target) {
                    edge.label = edge.label.split(', ')
                    edge.label.push(label)
                    edge.label = edge.label.sort().join(', ');
                }
            })
        } else {
            s1.graph.addEdge({
                id: edgeIdS1,
                label: label,
                usedLabel: label,
                source: source,
                target: target,
                size: 2,
            })
        }
        edgeIdS1++;
    }
    clickedNodeS1 = -1;
}
const onNodeClickS1 = (e) => {
    console.log('node Data: ', e.data.node)
    if ($('#edgeLabel').val() && e.data.captor.ctrlKey) {
        if (clickedNodeS1 !== -1) {
            addEdgeS1(clickedNodeS1, e.data.node.id, $('#edgeLabel').val());
        } else {
            clickedNodeS1 = e.data.node.id;
        }
    }
}
s1.bind('clickNode', onNodeClickS1);

$(document).ready(() => {
    s1.graph.addNode({
        id: "-1",
        x: -(window.innerWidth * 0.75 / 2 - 50),
        y: 0,
        size: 5,
        color: "grey",
    });
    const dl1 = new sigma.plugins.dragNodes(s1, s1.renderers[0]);
    $('#deleteStateButton1').on("click", deleteStateS1);
    $("#addStateButton1").on("click", addStateS1)
    $('#edgeLabel1').change(() => {
        clickedNodeS1 = -1;
    })
});