var graphTwo = new GraphState();
var clickedCoordinatesS2 = {};
var clickedNodeS2 = -1;

var nodeIdS2 = 0,
    selectedNodeS2 = null,
    edgeIdS2 = 0;

let s2 = new sigma({
    settings,
    renderer: {
        container: document.getElementById("canvasTwo"),
        type: "canvas",
    },
});

const addStateS2 = () => {
    if ($('#stateNameInput2').val() && graphTwo.addNode($('#stateNameInput2').val().trim(), $('input[name=stateOption2]:checked').val() == '#fb3189' ? 'final' : 'normal')) {
        s2.graph.addNode({
            id: nodeIdS2,
            label: $('#stateNameInput2').val().trim(),
            x: clickedCoordinatesS2.x,
            y: clickedCoordinatesS2.y,
            color: $('input[name=stateOption2]:checked').val(),
            size: 25,
        });
        nodeIdS2 === 0 &&
            s2.graph.addEdge({
                id: "-1",
                source: "-1",
                target: "0",
                size: 2,
                type: "arrow",
            }) && graphTwo.setInitialState($('#stateNameInput2').val().trim());
        selectedNodeS2 = null;
        $("#stateNameInput2").val("");
        $('#inputModal2').modal('hide');
    }
    nodeIdS2++;
}

const onBackgroundDoubleClickS2 = (e) => {
    clickedCoordinatesS2.x = e.data.captor.x;
    clickedCoordinatesS2.y = e.data.captor.y;
    $("#stateNameInput2").focus();
    $('#inputModal2').modal('show');
}
s2.bind('doubleClickStage', onBackgroundDoubleClickS2);

const deleteStateS2 = () => {
    const id = $('#stateName2').val();
    const name = $(`#stateName2 > option[value=${id}]`).html();
    if (id > 0) {
        console.log(graphTwo.dropNode(name))
        console.log(id)
        s2.graph.dropNode(parseInt(id));
        $('#deleteStateModal2').modal('hide');
    }
}

const onNodeDoubleClickS2 = (e) => {
    if (e.data.captor.ctrlKey)
        return;
    let options = '';
    s2.graph.nodes().forEach((node) => {
        if (node.id !== e.data.node.id && node.id > -2)
            options = options + `<option value=${node.id}>${node.label}</option>`
    });
    options += `<option value=${e.data.node.id} selected>${e.data.node.label}</option>`
    $('#stateName2').html(options);
    $('#deleteStateModal2').modal('show');
}
s2.bind('doubleClickNode', onNodeDoubleClickS2);

// ADD EDGE ON NODE CLICK
const addEdgeS2 = (source, target, label) => {
    if (graphTwo.addEdge(s2.graph.nodes(source).label, s2.graph.nodes(target).label, label)) {
        if (graphTwo.edgeRequireConcat(s2.graph.nodes(source).label, s2.graph.nodes(target).label)) {
            s2.graph.edges().forEach((edge) => {
                if (edge.source === source && edge.target === target) {
                    edge.label = edge.label.split(', ')
                    edge.label.push(label)
                    edge.label = edge.label.sort().join(', ');
                }
            })
        } else {
            s2.graph.addEdge({
                id: edgeIdS2,
                label: label,
                usedLabel: label,
                source: source,
                target: target,
                size: 2,
            })
        }
        edgeIdS2++;
    }
    clickedNodeS2 = -1;
}
const onNodeClickS2 = (e) => {
    if ($('#edgeLabel').val() && e.data.captor.ctrlKey) {
        if (clickedNodeS2 !== -1) {
            addEdgeS2(clickedNodeS2, e.data.node.id, $('#edgeLabel').val());
        } else {
            clickedNodeS2 = e.data.node.id;
        }
    }
}
s2.bind('clickNode', onNodeClickS2);

$(document).ready(() => {
    s2.graph.addNode({
        id: "-1",
        x: -(window.innerWidth * 0.75 / 2 - 50),
        y: 0,
        size: 5,
        color: "grey",
    });
    const dl2 = new sigma.plugins.dragNodes(s2, s2.renderers[0]);
    $('#deleteStateButton2').on("click", deleteStateS2);
    $("#addStateButton2").on("click", addStateS2)
    $('#edgeLabel2').change(() => {
        clickedNodeS2 = -1;
    })
});