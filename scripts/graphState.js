class GraphState {
  states = {};
  edges = [];

  setInitialState(nodeName) {
    this.initialState = nodeName;
  }

  addNode(nodeName, state) {
    if (!this.states[nodeName]) {
      this.states[nodeName] = {
        state: state,
        edges: {}
      }
      return true;
    }
    return false;
  }

  addEdge(from, to, label) {
    if (!this.states[from].edges[label]) {
      this.states[from].edges[label] = to;
      !this.edges.includes(label) && this.edges.push(label);
      return true;
    }
    return false;
  }

  dropNode(nodeName) {
    // Deleting Node
    delete this.states[nodeName];
    // Deleting Edges connected to Node
    Object.values(this.states).forEach((node) => {
      Object.keys(node.edges).forEach((edge) => {
        if (node.edges[edge] === nodeName)
          delete node.edges[edge];
      })
    })
  }

  dropEdge(from, value) {
    Object.keys(this.states[from].edges).forEach((edgeValue) => {
      if (edgeValue == value)
        delete this.states[from].edges[edgeValue];
    })
  }

  edgeRequireConcat(from, to) {
    let counter = 0;
    Object.keys(this.states[from].edges).length > 1 && Object.keys(this.states[from].edges).forEach((alphabet) => {
      if (this.states[from].edges[alphabet] == to) {
        counter++;
      }
    })
    return counter > 1 ? true : false;
  }

  static checkEquivalence(d1, d2) {
    let collectionsToVerify = [];
    let collectionsToVerifyNew = [];
    let collectionsVerified = [];
    let flag = true;
    try {
      collectionsToVerify.push([d1.initialState, d2.initialState]);
      do {
        collectionsToVerify.forEach((stateName) => {
          if (d1.states[stateName[0]].state !== d2.states[stateName[1]].state) {
            flag = false
          }
          if (flag && !collectionsVerified.includes(JSON.stringify([stateName[0], stateName[1]]))) {
            collectionsVerified.push(JSON.stringify([stateName[0], stateName[1]]))
            d1.edges.forEach((edge) => {
              let newCollection = [d1.states[stateName[0]].edges[edge], d2.states[stateName[1]].edges[edge]];
              if (!collectionsVerified.includes(JSON.stringify(newCollection))) {
                collectionsToVerifyNew.push(newCollection);
              }
            })
          }
        })
        collectionsToVerify = [...collectionsToVerifyNew];
        collectionsToVerifyNew = [];
      } while (flag && collectionsToVerify.length > 0);
    } catch(error) {
      return false;
    }
    return flag;
  }
}