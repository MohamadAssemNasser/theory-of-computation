const checkEquivalence = () => {
    alert(GraphState.checkEquivalence(graphOne, graphTwo))
}

// WATCHERS ON EVENTS ON GRAPH
sigma.classes.graph.attach("addNode", "incrementIDAndRefresh", () => {
    s1.refresh();
    s2.refresh();
});

sigma.classes.graph.attach(
    "addEdge",
    "incrementIDAndMergeAndRefresh",
    () => {
        s1.refresh();
        s2.refresh();
    }
);

sigma.classes.graph.attach("dropNode", "deleteNodeAndRefresh", () => {
    s1.refresh();
    s2.refresh();
})

$(window).on("contextmenu", () => false);

$(document).ready(() => {
    s1.refresh();
    s2.refresh();
    $('#checkEquivalence').on('click', checkEquivalence)
});